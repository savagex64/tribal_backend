import { FindManyOptions, FindOneOptions } from 'typeorm';

export interface IGenericService<T> {
	getAll(options: FindManyOptions): Promise<T[]>;

	get(id: string,options?: FindOneOptions): Promise<T>;

	update(entity: T): Promise<T>;

	create(entity:T): Promise<T>;

	delete(id: string);
}
