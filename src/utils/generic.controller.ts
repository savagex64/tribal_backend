import { FindManyOptions, FindOneOptions, ObjectLiteral } from 'typeorm';
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { IGenericService } from './interfaces/IGeneric.service';

@Controller()
export class GenericController<T extends ObjectLiteral> {
	constructor(private readonly _service: IGenericService<T>) {
	}

	@Get()
	@ApiResponse({ status: 200, description: 'Ok' })
	async findAll(options?: FindManyOptions): Promise<T[]> {
		return this._service.getAll(options);
	}

	@Get(':id')
	@ApiResponse({ status: 200, description: 'Entity retrieved successfully.' })
	@ApiResponse({ status: 404, description: 'Entity does not exist' })
	async findById(@Param('id') id: string, options?: FindOneOptions): Promise<T> {
		return await this._service.get(id, { ...options, order: { id: 'DESC' } });
	}

	@Post()
	@ApiResponse({ status: 201, description: 'The record has been successfully created.' })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	@ApiResponse({ status: 400, description: 'Bad Request.' })
	async create(@Body() entity: T): Promise<T> {
		console.log();
		return await this._service.create(entity);
	}

	@Delete(':id')
	@ApiResponse({ status: 200, description: 'Entity deleted successfully.' })
	@ApiResponse({ status: 400, description: 'Bad Request.' })
	async delete(@Param('id') id: string) {
		this._service.delete(id);
	}

	@Put()
	@ApiResponse({ status: 400, description: 'Bad Request.' })
	@ApiResponse({ status: 200, description: 'Entity deleted successfully.' })
	async update(@Body() entity: any): Promise<T> {
		return await this._service.update(entity);
	}
}
