import { FindManyOptions, FindOneOptions, ObjectLiteral, Repository } from 'typeorm';
import { BadGatewayException, BadRequestException, Injectable } from '@nestjs/common';
import { IGenericService } from './interfaces/IGeneric.service';

class Pagination<T> {}

@Injectable()
export abstract class GenericService<T extends ObjectLiteral> implements IGenericService<T> {
	constructor(readonly repository: Repository<T>) {}

	// async paginate(
	//   page = 1,
	//   options?: FindConditions<T> | FindManyOptions,
	//   limit?: 10,
	// ): Promise<Pagination<T>> {
	//   return await paginate(this.repository, { page, limit }, options);
	// }

	async create(entity: any): Promise<T> {
		return await this.repository.save({ ...entity });
	}

	getAll(options?: FindManyOptions): Promise<T[]> {
		try {
			return <Promise<T[]>>this.repository.find(options);
		} catch (error) {
			throw new BadGatewayException(error);
		}
	}

	async get(id: string, options?: FindOneOptions): Promise<T> {
		try {
		} catch (error) {
			throw new BadGatewayException(error);
		}
		return await this.repository.findOne(id, options);
	}

	delete(id: string) {
		try {
			this.repository.delete(id);
		} catch (error) {
			throw new BadGatewayException(error);
		}
	}

	async update(entity: any): Promise<any> {
		const obj = await this.repository.findOne(entity.id);
		if (!obj) {
			new BadRequestException("resource doesn't exist");
		}
		return await this.repository.save(entity);
	}
}
