import { ObjectIdColumn, PrimaryGeneratedColumn } from 'typeorm';

export class GenericEntity {
  @ObjectIdColumn()
  _id: string;
}