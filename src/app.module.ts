import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { RestaurantModule } from './modules/restaurant/restaurant.module';
import { AuthModule } from './modules/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';

import * as ORMConfig from '../ormconfig.js';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { StorageModule } from './modules/storage/storage.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import * as path from 'path';

@Module({
	imports: [
		UserModule,
		RestaurantModule,
		AuthModule,
		TypeOrmModule.forRoot(ORMConfig),
		StorageModule,
		ServeStaticModule.forRoot({
			rootPath: path.join(__dirname, '..'),
			exclude: ['/api*'],
			// serveRoot: path.join(__dirname, 'storage', 'images'),
		}),
	],
	controllers: [AppController],
	providers: [AppService, LoggingInterceptor],
})
export class AppModule {}
