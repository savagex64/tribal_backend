import { Module } from '@nestjs/common';
import { RestaurantService } from './restaurant.service';
import { RestaurantController } from './restaurant.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Restaurant } from './entities/restaurant.entity';
import { CommentaryService } from './commentary.service';
import { Comment } from './entities/comment.entity';

@Module({
	controllers: [RestaurantController],
	providers: [RestaurantService, CommentaryService],
	imports: [TypeOrmModule.forFeature([Restaurant, Comment])],
	exports: [RestaurantService],
})
export class RestaurantModule {}
