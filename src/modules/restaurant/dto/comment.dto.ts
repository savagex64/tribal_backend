import { PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { Restaurant } from '../entities/restaurant.entity';

export class CreateCommentDTO {

	@IsString()
	@IsNotEmpty()
	commentary: string;

	@IsNotEmpty()
	user: string;

	@IsNotEmpty()
	restaurant: Restaurant;
}

export class UpdateCommentDTO extends PartialType(CreateCommentDTO) {
	@IsNotEmpty()
	id: string;
}
