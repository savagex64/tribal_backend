import { IsNotEmpty, IsString } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';

export class CreateRestaurantDTO {
	@IsString()
	name: string;
	@IsString()
	description: string;

	@IsNotEmpty({ message: 'Locacion esta vacia' })
	location: string;
}

export class UpdateRestaurantDTO extends PartialType(CreateRestaurantDTO) {
	@IsNotEmpty()
	id: string;
}
