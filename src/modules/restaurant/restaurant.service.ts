import { Injectable } from '@nestjs/common';
import { GenericService } from '../../utils/generic.service';
import { Restaurant } from './entities/restaurant.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RestaurantService extends GenericService<Restaurant> {
	constructor(@InjectRepository(Restaurant) readonly repository: Repository<Restaurant>) {
		super(repository);
	}


}
