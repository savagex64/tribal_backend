import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { RestaurantService } from './restaurant.service';
import { GenericController } from '../../utils/generic.controller';
import { Restaurant } from './entities/restaurant.entity';
import { CommentaryService } from './commentary.service';
import { CreateCommentDTO } from './dto/comment.dto';
import { FindOneOptions } from 'typeorm';

@Controller('restaurant')
export class RestaurantController extends GenericController<Restaurant> {
	constructor(private readonly service: RestaurantService, private readonly commentaryService: CommentaryService) {
		super(service);
	}

	@Post('comment')
	async createCommentary(@Body() body: CreateCommentDTO) {
		return await this.commentaryService.create(body);
	}

	@Get(':id/comment')
	async getCommentariesByRestaurant(@Param('id') restaurantId: string) {
		return await this.commentaryService.getAll({ where: { restaurant: restaurantId }, order: { id: 'DESC' } });
	}

	@Get(':id')
	async findById(@Param('id') id: string, options?: FindOneOptions): Promise<Restaurant> {
		const data = await this.service.get(id, options);
		data.comments = await this.commentaryService.getAll({ where: { restaurant: data.id }, order: { id: 'DESC' } });
		return data;
	}

	@Get(':id/get-data')
	async findByIdOrder(@Param('id') id: string, options?: FindOneOptions): Promise<Restaurant> {
		const data = await this.service.get(id, {relations:['files']});
		data.comments = await this.commentaryService.getAll({ where: { restaurant: data.id }, order: { createdAt: 'DESC' } });
		return data;
	}
}
