import { Injectable } from '@nestjs/common';
import { GenericService } from '../../utils/generic.service';
import { Comment } from './entities/comment.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CommentaryService extends GenericService<Comment> {

	constructor(@InjectRepository(Comment) readonly repository: Repository<Comment>) {
		super(repository);
	}
}
