import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Restaurant } from './restaurant.entity';

@Entity('commentaries')
export class Comment {
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Column()
	commentary: string;

	@ManyToOne(() => Restaurant, (restaurant) => restaurant.comments)
	restaurant: Restaurant;

	@Column()
	user: string;

	@Column()
	@CreateDateColumn()
	createdAt: Date;
}
