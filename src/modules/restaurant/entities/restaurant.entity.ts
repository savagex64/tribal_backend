import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Comment } from './comment.entity';
import { Storage } from '../../storage/entities/storage.entity';

@Entity('restaurants')
export class Restaurant {
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Column()
	name: string;
	@Column()
	description: string;
	@Column({ nullable: true })
	location: string;

	@Column()
	@CreateDateColumn()
	createdAt: Date;

	@OneToMany(() => Comment, (comment) => comment.restaurant, { eager: true })
	comments?: Array<Comment>;

	@OneToMany(() => Storage, (storage) => storage.restaurant, { eager: true })
	files?: Array<Restaurant>;
}
