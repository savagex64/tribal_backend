import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Comment } from '../../restaurant/entities/comment.entity';

@Entity('users')
export class User {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column()
	name: string;

	@Column()
	username: string;

	@Column()
	password: string;

	@Column()
	email: string;

	@CreateDateColumn()
	createdAt: Date;

	@OneToMany(() => Comment, (comment) => comment.user)
	comments: Comment;
}
