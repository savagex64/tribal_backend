import { PartialType } from '@nestjs/mapped-types';
import { IsAlpha, IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class CreateUserDto {
	@IsString()
	@ApiModelProperty()
	@IsNotEmpty({ message: 'The name is required' })
	username: string;

	@IsAlpha()
	@ApiModelProperty()
	@IsNotEmpty()
	password: string;

	@IsString()
	@ApiModelProperty()
	@IsNotEmpty()
	name: string;

	@IsEmail()
	@ApiModelProperty()
	@IsNotEmpty()
	email: string;
}

export class UpdateUserDTO extends PartialType(CreateUserDto) {
	@IsString()
	@IsNotEmpty()
	id: string;

	@IsNotEmpty()
	username: string;
}
