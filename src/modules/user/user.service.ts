import { Injectable } from '@nestjs/common';
import { CreateUserDto, UpdateUserDTO } from './dto/user.dto';
import { GenericService } from '../../utils/generic.service';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService extends GenericService<User> {
	constructor(@InjectRepository(User) private readonly repo: Repository<User>) {
		super(repo);
	}
}
