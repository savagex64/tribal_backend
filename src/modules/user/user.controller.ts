import { Controller, Delete, Post, Put } from '@nestjs/common';
import { GenericController } from '../../utils/generic.controller';
import { User } from './entities/user.entity';
import { CreateUserDto, UpdateUserDTO } from './dto/user.dto';
import { UserService } from './user.service';
import { ApiCreatedResponse, ApiResponse } from '@nestjs/swagger';

@Controller('user')
export class UserController extends GenericController<User> {
	constructor(private readonly service: UserService) {
		super(service);
	}

	@Post()
	@ApiResponse({ status: 201, description: 'The record has been successfully created.' })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	@ApiResponse({ status: 400, description: 'Bad Request.' })
	@ApiCreatedResponse({ type: CreateUserDto })
	async create(entity: CreateUserDto): Promise<User> {
		return super.create(<User>entity);
	}

	@Put()
	async update(entity: UpdateUserDTO): Promise<User> {
		return super.update(entity);
	}
}
