import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Restaurant } from '../../restaurant/entities/restaurant.entity';

@Entity()
export class Storage {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column()
	url: string;

	@Column({ nullable: true })
	description: string;

	@Column()
	@CreateDateColumn()
	createdAt: Date;

	@ManyToOne(() => Restaurant, (restaurant) => restaurant.files)
	restaurant: Array<Restaurant>;
}
