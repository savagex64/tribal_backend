import { IsAlphanumeric, IsNotEmpty, IsString } from 'class-validator';

export class CreateStorageDto {
	url: string;

	@IsString()
	description: string;

	@IsNotEmpty()
	restaurant: string;
}
