import { Module } from '@nestjs/common';
import { StorageService } from './storage.service';
import { StorageController } from './storage.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Storage } from './entities/storage.entity';
import { RestaurantModule } from '../restaurant/restaurant.module';
import { RestaurantService } from '../restaurant/restaurant.service';
import { Restaurant } from '../restaurant/entities/restaurant.entity';
import { Comment } from '../restaurant/entities/comment.entity';

@Module({
	imports: [TypeOrmModule.forFeature([Storage, Restaurant, Comment]), RestaurantModule],
	controllers: [StorageController],
	providers: [StorageService, RestaurantService],
})
export class StorageModule {}
