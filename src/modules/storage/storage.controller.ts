import { BadRequestException, Body, Controller, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { StorageService } from './storage.service';
import { GenericController } from '../../utils/generic.controller';
import { Storage } from './entities/storage.entity';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import e from 'express';
import * as UUID from 'uuid';
import * as path from 'path';
import { CreateStorageDto } from './dto/create-storage.dto';
import { RestaurantService } from '../restaurant/restaurant.service';

@Controller('storage')
export class StorageController extends GenericController<Storage> {
	constructor(readonly storageService: StorageService, readonly restaurantService: RestaurantService) {
		super(storageService);
	}

	@Post('upload')
	@UseInterceptors(
		FileInterceptor('file', {
			storage: diskStorage({
				destination: './storage/images/',
				filename(req: e.Request, file: Express.Multer.File, callback: (error: Error | null, filename: string) => void) {
					const filename = UUID.v4();
					const extension = path.parse(file.originalname).ext;
					callback(null, `${filename}${extension}`);
				},
			}),
		})
	)
	async upload(@UploadedFile() file: Express.Multer.File, @Body() obj: CreateStorageDto) {
		obj.url = file.path;
		console.log(obj.url);
		const restaurant = await this.restaurantService.get(obj.restaurant);
		if (restaurant) return await this.storageService.create(obj);

		return new BadRequestException();
	}
}
