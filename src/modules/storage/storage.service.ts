import { Injectable, Post, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { GenericService } from '../../utils/generic.service';
import { Storage } from './entities/storage.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FilesInterceptor } from '@nestjs/platform-express';

@Injectable()
export class StorageService extends GenericService<Storage> {
	constructor(@InjectRepository(Storage) readonly repository: Repository<Storage>) {
		super(repository);
	}


}
