import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class CreateAuthDto {
	@ApiModelProperty()
	username: string;
	@ApiModelProperty()
	password: string;
}
