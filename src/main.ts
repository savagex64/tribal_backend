import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import 'reflect-metadata';
import { ValidationPipe } from '@nestjs/common';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { Logger } from './utils/logger/logger';

async function bootstrap() {
	const app = await NestFactory.create(AppModule, { logger: new Logger() });
	const options = new DocumentBuilder().setTitle('Tribal MnC test API').setDescription('API').setVersion('1.0').addTag('generic-api').build();
	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup('/api/', app, document);

	app.setGlobalPrefix('api');
	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true,
		})
	);
	app.enableCors();
	app.useGlobalInterceptors(new LoggingInterceptor());
	await app.listen(3000);
}

bootstrap();
