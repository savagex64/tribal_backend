import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Http2ServerRequest } from 'http2';
import { Logger } from '../utils/logger/logger';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
	intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		const mylogger = new Logger();
		const route: Http2ServerRequest = context.switchToHttp().getRequest();
		mylogger.debug('Route called: ' + route.url + ' with method ' + route.method);

		return next.handle();
	}
}
