

#Ejecutar  en CMD : make up     para correr el proyecto en producción
up:
	yarn
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose up -d --build


restart:
	docker-compose restart

logs:
	docker-compose logs

backend:
	docker logs ac_api

reboot:
	make down
	make up


prod:
	yarn
	yarn build
	yarn start:prod
	docker-compose -f docker-compose.prod.yml up -d

prod-restart:
	docker-compose -f docker-compose.prod.yml down
	docker-compose -f docker-compose.prod.yml build
	docker-compose -f docker-compose.prod.yml up -d